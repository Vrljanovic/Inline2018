﻿using System;

namespace Inline2018
{
    class ParseException : Exception
    {
        public ParseException(string Message) : base(Message) { }
    }      
}