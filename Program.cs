﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Inline2018
{
    class Program
    {
        public const string NewFile = "tempFile.txt";
        public static StreamWriter Writer = new StreamWriter(NewFile);
        public static Regex IncludeRegex = new Regex(Analyzer.Include);
        public static string FirstFile = string.Empty;
        public static void IncludeFiles(string path, string oldPath)  //Making one file program file
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("Error: " + path + " does not exist.");
            string[] lines = File.ReadAllLines(path);
            foreach (string line in lines)
            {
                if (IncludeRegex.Match(line).Success)
                {
                    string newPath = IncludeRegex.Match(line).Groups[1].Value;
                    if (oldPath != null && oldPath == newPath || newPath == path || newPath == FirstFile)
                        throw new ParseException("Error: Cyclic Including");
                        IncludeFiles(newPath, path);
                }
                else
                {
                    if (line.Trim() != "")
                        Writer.WriteLine(line);
                }
            }
        }
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Writer.Close();
                File.Delete(NewFile);
                Console.WriteLine("Error! Expected filename as argument"); // First argument is file
                Environment.Exit(-1);
            }
            try
            {
                FirstFile = args[0];
                IncludeFiles(args[0], null);
                Writer.Close();
                string[] code = File.ReadAllLines(NewFile);
                File.Delete(NewFile);
                foreach (string line in code)
                    Analyzer.Analyze(line);
            }
            catch (Exception ex)
            {
                Writer.Close();
                Console.WriteLine("\n" + ex.Message);
                File.Delete(NewFile);
            }
        }
    }
}
