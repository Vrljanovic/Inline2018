﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace Inline2018
{
    static class Analyzer
    {
        private const string Var = @"([[]{1}[a-zA-Z][a-zA-Z0-9#_$.]*[]])"; //Regular: [var1#$._]
        private const string IntegerOperator = @"([+-/%///*])";
        private const string StringOperator = @"+";
        private const string NumericalExpression = @"\(*[+-]?\(*(" + Var + "|[0-9]+)" + @"([ \t]*" + IntegerOperator + @"[ \t]*" + @"\(*(" + Var + "|[0-9]+)" + @")*[ \t]*[\)]*" + @"([ \t]*" + IntegerOperator + @"[ \t]*" + @"\(*(" + Var + "|[0-9]+)" + @")*[ \t]*[\)]*"; // [var] = -[var]+([var2]*([var3]+[var4]))
        public const string Include = "^include (.*)";
        private const string AllCaps = "<all_caps>[a-zA-Z0-9#$._ \t]*</all_caps>";
        private const string StringLiteral = "\"([a-zA-Z0-9._#$ \t]*)\"";
        private const string StringAndNumber = StringLiteral + "[ \t]*\\+[ \t]*[0-9]+|[0-9]+[ \t]*\\+[ \t]*" + StringLiteral;
        private const string StringAndVar = StringLiteral + "[ \t]*\\+[ \t]*" + Var + "|" + Var + "[ \t]*\\+[ \t]*" + StringLiteral;
        private const string StringExpression = StringLiteral + "([ \t]*\\+{0,1}" + StringLiteral + ")*|" + StringAndNumber + "|" + StringAndVar;
        private const string StringDeclaration = @"^([ \t]*" + Var + @")[ \t]*" + "(=)" + @"[ \t]*" + StringExpression + @"$";  //[var1] = "This is string"
        private const string NumericalDeclaration = @"^([ \t]*" + Var + @")[ \t]*" + "(=)" + @"[ \t]*" + NumericalExpression + @"$"; //[var1] = ([-var2])*[var3]/[var4]
        private const string StringInline = "@{" + Var + @"[ \t]*" + "(=)" + @"[ \t]*" + StringLiteral + "}";
        private const string NumericalInline = "@{" + Var + @"[ \t]*" + "(=)" + @"[ \t]*" + NumericalExpression + "}";
        private const string InlineExpression = StringInline + "|" + NumericalInline;
        private const string Text = @"^[^\\[].*"; // Catch every line which is not declaration
        private const string PrintOnConsole = "=" + Var;
        private const string PossibleInline = "@{.*}";
        private const string TextInline = "@{[a-zA-Z0-9#$._ \t]*}";

        private static HashSet<NumericalVariable> NumericalVariables = new HashSet<NumericalVariable>();
        private static HashSet<StringVariable> StringVariables = new HashSet<StringVariable>();
        private static NumericalVariable ContainsNumericalVariable(string variableName)
        {
            foreach (NumericalVariable var in NumericalVariables)
            {
                if (var.VariableName.Equals(variableName))
                    return var;
            }
            return null;
        }
        private static StringVariable ContainsStringVariable(string variableName)
        {
            foreach (StringVariable var in StringVariables)
            {
                if (var.VariableName.Equals(variableName))
                    return var;
            }
            return null;
        }

        private static Regex StringRegex = new Regex(StringDeclaration);
        private static Regex NumericalRegex = new Regex(NumericalDeclaration);
        private static Regex TextRegex = new Regex(Text);

        public static void Analyze(string line)
        {
            if (StringRegex.Match(line).Success || IsStringExpression(line))
            {
                ExecuteStringExpression(line);
            }
            else if (NumericalRegex.Match(line).Success)
            {
                ExecuteNumericalExpression(line);
            }
            else if (TextRegex.Match(line).Success)
            {
                string println = line;
                Regex inlineRegex = new Regex(InlineExpression);
                Regex printRegex = new Regex(PrintOnConsole);
                Regex allCapsRegex = new Regex(AllCaps);
                if (new Regex(PossibleInline).Match(line).Success)
                {
                    MatchCollection possibleInlineCollection = new Regex(PossibleInline).Matches(line);
                    foreach (Match match in possibleInlineCollection)
                    {
                        if (!new Regex(NumericalInline).Match(match.Value).Success && !new Regex(StringInline).Match(match.Value).Success && !new Regex(TextInline).Match(line).Success)
                            throw new ParseException("Error in line: " + line + "\n" + match.Value + " is not correct inline.");
                    }
                }
                else
                    println = line;
                if (inlineRegex.Match(line).Success)
                {
                    MatchCollection inlineCollections = inlineRegex.Matches(line);
                    foreach (Match match in inlineCollections)
                    {
                        println = line.Replace(inlineRegex.Match(line).Value, string.Empty);
                        string expr = match.Value;
                        expr = expr.Replace("@{", string.Empty);
                        expr = expr.Replace("}", string.Empty);
                        if (new Regex(NumericalDeclaration).Match(expr).Success)
                            ExecuteNumericalExpression(expr);
                        else if (new Regex(StringDeclaration).Match(expr).Success)
                            ExecuteStringExpression(expr);
                        else
                            throw new ParseException("Error in line: " + line + "\nWrong Inline Code");
                    }
                }
                if (printRegex.Match(line).Success)
                {
                    MatchCollection toPrintCollection = printRegex.Matches(line);
                    foreach (Match match in toPrintCollection)
                    {
                        string varName = match.Value.Substring(1);
                        StringVariable var = ContainsStringVariable(varName);
                        if (var != null)
                            println = println.Replace(match.Value, var.VariableValue);
                        NumericalVariable numVar = ContainsNumericalVariable(varName);
                        if (numVar != null)
                            println = println.Replace(match.Value, Convert.ToString(numVar.VariableValue));
                        if (var == null && numVar == null)
                            throw new ParseException("Error in line: " + line + "\n" + varName + " is not initialized.");
                    }
                }
                if (allCapsRegex.Match(line).Success)
                {
                    MatchCollection allCapsCollection = allCapsRegex.Matches(println);
                    foreach (Match match in allCapsCollection)
                    {
                        string replaceString = match.Value;
                        replaceString = replaceString.Replace("<all_caps>", string.Empty);
                        replaceString = replaceString.Replace("</all_caps>", string.Empty);
                        println = println.Replace(match.Value, replaceString.ToUpper());
                    }
                }
                if (new Regex(TextInline).Match(line).Success)
                {
                    MatchCollection textInlineCollection = new Regex(TextInline).Matches(line);
                    foreach (Match match in textInlineCollection)
                    {
                        string replaceString = match.Value.Replace("@{", string.Empty);
                        replaceString = replaceString.Replace("}", string.Empty);
                        println = println.Replace(match.Value, replaceString);
                    }
                }
                Console.WriteLine(println);
            }
            else
            {
                throw new ParseException("Error in line: " + line);
            }
        }
        private static string ReplaceNumericalVariableNames(string expression)
        {
            MatchCollection varCollection = new Regex(Var).Matches(expression);
            for (int i = 1; i < varCollection.Count; ++i)
            {
                NumericalVariable numVar = ContainsNumericalVariable(varCollection[i].Value);
                expression = expression.Replace(numVar.VariableName, Convert.ToString(numVar.VariableValue));
            }
            return expression;
        }
        private static void ExecuteNumericalExpression(string expression)
        {
            Regex varRegex = new Regex(Var);
            MatchCollection varCollection = varRegex.Matches(expression);
            string variableName = varCollection[0].Value;
            if (ContainsStringVariable(variableName) != null)
                throw new ParseException("Errorine line: " + expression + "\n" + varCollection[0].Value + " is String.");
            string line = expression.Replace(" ", string.Empty);
            line = line.Replace("\t", string.Empty);
            for (int i = 1; i < varCollection.Count; ++i)
            {
                if (ContainsNumericalVariable(varCollection[i].Value) == null)
                    throw new ParseException("Error in line : " + expression + "\n" + varCollection[i].Value + " is not initialzied.");
            }
            string expr = ReplaceNumericalVariableNames(line);
            expr = expr.Substring(expr.IndexOf("=") + 1);
            int value = CalculateNumericalExpression(expr);
            NumericalVariable var = ContainsNumericalVariable(variableName);
            if (var != null)
                var.VariableValue = value;
            else
            {
                NumericalVariable variable = new NumericalVariable(variableName, value);
                NumericalVariables.Add(variable);
            }
        }
        private static int CalculateNumericalExpression(string expression)
        {
            var dataTable = new DataTable();
            var dataColumn = new DataColumn("Eval", typeof(int), expression);
            dataTable.Columns.Add(dataColumn);
            dataTable.Rows.Add(0);
            return (int)(dataTable.Rows[0]["Eval"]);
        }
        private static bool IsStringExpression(string expression) //For those lines which can be both Numerical and String
        {
            if (new Regex(Text).Match(expression).Success)
                return false;
            MatchCollection varCollection = new Regex(Var).Matches(expression);
            if (varCollection.Count != 0)
            {
                if (ContainsNumericalVariable(varCollection[0].Value) != null)
                    return false;
                else if (ContainsStringVariable(varCollection[0].Value) != null)
                {
                    if (new Regex(Var).Matches(expression).Count == 1 && !new Regex(StringLiteral).Match(expression).Success)
                        throw new ParseException("Error in line: " + expression + "\n" + varCollection[0] + " is string. Right side is not valid string.");
                    return true;
                }
            }
            else
            {
                string rightSide = expression.Substring(expression.IndexOf("=") + 1);
                if (new Regex(StringLiteral).Match(rightSide).Success)
                    return true;
                if (new Regex(Var).Match(rightSide).Success)
                {
                    for (int i = 1; i < varCollection.Count; ++i)
                    {
                        StringVariable var = ContainsStringVariable(varCollection[i].Value);
                        if (var != null)
                            return true;
                    }
                    return false;
                }
            }
            return false;
        }
        private static void ExecuteStringExpression(string expression)
        {
            MatchCollection operatorCollection = new Regex(IntegerOperator).Matches(expression);
            foreach (Match match in operatorCollection)
            {
                if (match.Value != "+")
                    throw new ParseException("Error in line: " + expression + "\n\"" + match.Value + "\" is not string operator.");
            }
            if (expression.IndexOf("=") < 0)
                throw new ParseException("Error in line: " + expression + "\nWrong Expression.");
            string rightSide = expression.Substring(expression.IndexOf("=") + 1);
            MatchCollection collection = new Regex(Var + "|" + StringLiteral + "|[0-9]+").Matches(rightSide);
            string varName = new Regex(Var).Matches(expression)[0].Value;
            string value = string.Empty;
            if (ContainsNumericalVariable(varName) != null)
                throw new ParseException("Error in line: " + expression + "\n" + varName + " is Integer.");
            foreach (Match match in collection)
            {
                if (new Regex(Var).Match(match.Value).Success)
                {
                    if (ContainsNumericalVariable(match.Value) != null)
                    {
                        NumericalVariable var = ContainsNumericalVariable(match.Value);
                        value += Convert.ToString(var.VariableValue);
                    }
                    else if (ContainsStringVariable(match.Value) != null)
                    {
                        StringVariable var = ContainsStringVariable(match.Value);
                        value += var.VariableValue;
                    }
                    else
                        throw new ParseException("Error in line: " + expression + "\n" + match.Value + " is not initialized.");
                }
                else if (new Regex(StringLiteral).Match(rightSide).Success)
                {
                    string temp = match.Value.Replace("\"", string.Empty);
                    value += temp;
                }
                else
                {
                    string temp = match.Value.Replace(" ", string.Empty);
                    temp = temp.Replace("\t", string.Empty);
                    value += temp;
                }
            }
            StringVariable variable = ContainsStringVariable(varName);
            if (variable != null)
                variable.VariableValue = value;
            else
                StringVariables.Add(new StringVariable(varName, value));
        }
    }
}