﻿namespace Inline2018
{
    class StringVariable
    {
        public string VariableName { get; set; }
        public string VariableValue { get; set; }
        public StringVariable(string name, string value)
        {
            VariableName = name;
            VariableValue = value;
        }
        public StringVariable()
        {
            VariableValue = string.Empty;
            VariableName = string.Empty;
        }
        public override int GetHashCode()
        {
            return VariableName.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            else
            {
                StringVariable temp = (StringVariable)obj;
                return temp.VariableName.Equals(VariableName) && temp.VariableValue.Equals(VariableValue);
            }
        }
        public override string ToString()
        {
            return VariableName + "=" + VariableValue;
        }
    }
}