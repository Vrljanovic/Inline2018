﻿using System;

namespace Inline2018
{
    public class NumericalVariable
    {
        public string VariableName { get; set; }
        public int VariableValue { get; set; }

        public NumericalVariable(String name, int value)
        {
            VariableValue = value;
            VariableName = name;
        }
        public NumericalVariable()
        {
            VariableValue = 0;
            VariableName = string.Empty;
        }
        public override int GetHashCode()
        {
            return VariableName.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            else
            {
                NumericalVariable temp = (NumericalVariable)obj;
                return temp.VariableName.Equals(VariableName) && temp.VariableValue == VariableValue;
            }
        }
        public override string ToString()
        {
            return VariableName + " = " + VariableValue;
        }
    }
}
